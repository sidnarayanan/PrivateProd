#!/bin/bash

modelclass=$1
medmass=$2
dmmass=$3
mgname=${1}_${2}_${3}

echo "Executable  = run.sh" > submit.condor
echo "Universe  = vanilla" >> submit.condor
echo 'Error = /scratch/snarayan/condor/logs/'${mgname}'.err' >> submit.condor
echo 'Output  = /scratch/snarayan/condor/logs/'${mgname}'.out' >> submit.condor
echo 'Log = /scratch/snarayan/condor/logs/'${mgname}'.log' >> submit.condor
echo 'Input = /dev/null' >> submit.condor
echo 'GetEnv = True' >> submit.condor
echo 'Arguments = "$(Process) '${modelclass} ${medmass} ${dmmass}'"' >> submit.condor
echo 'should_transfer_files = YES' >> submit.condor
echo 'when_to_transfer_output = ON_EXIT_OR_EVICT' >> submit.condor
echo '+AccountingGroup = "group_cmsuser.snarayan"' >> submit.condor
echo 'Queue 10' >> submit.condor

condor_submit submit.condor
