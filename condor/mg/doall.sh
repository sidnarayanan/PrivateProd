#!/bin/bash

condor_rm snarayan

for mchi in 50 200; do
  for mmed in 900 1100 1300 1700 1900; do
    ./submit.sh resonant $mmed $mchi
  done
done


