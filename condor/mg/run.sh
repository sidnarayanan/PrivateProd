#!/bin/bash

cd /home/snarayan/cms/cmssw/042/CMSSW_7_4_15/src
cmsenv
cd -

cp /home/snarayan/scratch5/PrivateProd/condor/mg/run.py .
python run.py $1 $2 $3 $4
