#!/usr/bin/env python

from sys import argv
from os import system

job = int(argv[1])
modelClass = argv[2]
medMass = int(argv[3])
dmMass = int(argv[4])

outdir = "/mnt/hscratch/snarayan/mc/monotop_%s/Med%i_Chi%i/"%(modelClass,medMass,dmMass)
system('mkdir -p '+outdir)

mgPath = '/scratch5/snarayan/MG5_aMC_v2_3_3/bin/%s.tgz'%(modelClass)
system('cp %s .'%(mgPath))
system('tar -xf %s.tgz'%(modelClass))

system("sed -i 's/.*= iseed/%i=iseed/' %s/Cards/run_card.dat"%(job+1,modelClass))
if modelClass=='fcnc':
  system("sed -i 's/.*MV/   32 %f # MV/' %s/Cards/param_card.dat"%(float(medMass),modelClass))
  system("sed -i 's/.*WV/DECAY  32 %f # WV/' %s/Cards/param_card.dat"%(float(medMass)/50,modelClass))
  system("sed -i 's/.*Mpsi/  1000023 %f # Mpsi/' %s/Cards/param_card.dat"%(float(dmMass),modelClass))
else:
  system("sed -i 's/.*Mpi/ 1000006 %f # Mpi/' %s/Cards/param_card.dat"%(float(medMass),modelClass))
  system("sed -i 's/.*WSC/DECAY 1000006 %f # WSC/' %s/Cards/param_card.dat"%(float(medMass)/100,modelClass))
  system("sed -i 's/.*MFM/  1000022 %f # MFM/' %s/Cards/param_card.dat"%(float(dmMass),modelClass))

system('./%s/bin/generate_events'%(modelClass))
system('rm -r %s/*_%i'%(outdir,job))
system('gunzip %s/Events/run01/unweighted_events.lhe.gz'%(modelClass))
system('cp -r %s/Events %s/Events_%i'%(modelClass,outdir,job))
system('cp -r %s/Cards %s/Cards_%i'%(modelClass,outdir,job))
system('rm -r %s'%(modelClass))
