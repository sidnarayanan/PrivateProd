#!/usr/bin/env python

import particles

names = {}
for k,v in particles.__dict__.iteritems():
  if type(particles.a)==type(v):
    names[v.pdg_code]=v.name
    if not v.selfconjugate:
      names[-1*v.pdg_code]=v.antiname

#print names

ftempl = open('../../input/particles_name_default.txt')
fout = open('particles_name.txt','w')

for line in ftempl:
  if line.strip()[0]=='#':
    fout.write(line)
  else:
    pdgid = int(line.split()[0])
    if pdgid in names:
      fout.write('%8i     %s\n'%(pdgid,names[pdgid]))
    else:
      fout.write(line)

fout.close()
